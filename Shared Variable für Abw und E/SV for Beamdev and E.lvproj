﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="varPersistentID:{5FDEDF6E-E1AB-48EE-BC42-7090D8312C07}" Type="Ref">/Mein Computer/SV for Beamdev and E.lvlib/Abwechung_und_E_speichern</Property>
	<Property Name="varPersistentID:{9759FE27-7712-48B4-B9C4-08CA2F0E4E48}" Type="Ref">/Mein Computer/SV for Beamdev and E.lvlib/LogShotID</Property>
	<Item Name="Mein Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SV for Beamdev and E.lvlib" Type="Library" URL="../SV for Beamdev and E.lvlib"/>
		<Item Name="Abhängigkeiten" Type="Dependencies"/>
		<Item Name="Build-Spezifikationen" Type="Build"/>
	</Item>
</Project>
